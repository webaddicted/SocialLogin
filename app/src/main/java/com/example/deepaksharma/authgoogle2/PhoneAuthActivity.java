package com.example.deepaksharma.authgoogle2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sociallogin.Interface.OnLoginListener;
import com.example.sociallogin.Model.LoginResponse;
import com.example.sociallogin.PhoneAuth.PhoneAuth;
import com.example.sociallogin.SocialLogin;

public class PhoneAuthActivity extends AppCompatActivity implements OnLoginListener {
    private EditText edtMobileNo;
    private EditText edtCode;
    TextView txt_Result;
    private static final String TAG = PhoneAuthActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        txt_Result = findViewById(R.id.txt_Result);
        edtMobileNo = findViewById(R.id.phone);
        edtCode = findViewById(R.id.phone_auth_code);
        PhoneAuth.phoneAuthCallBack(PhoneAuthActivity.this, this);
    }

    public void onSendOTP(View v) {
        PhoneAuth.sendOTP(edtMobileNo.getText().toString());
    }

    public void onReSendOTP(View v) {
        PhoneAuth.resendOTP(edtMobileNo.getText().toString());
    }

    public void onVerifyOTP(View v) {
        PhoneAuth.verifyOTP(edtCode.getText().toString());
    }

    public void onLogout(View v) {
        SocialLogin.signOut();
    }

    @Override
    public void onSuccess(LoginResponse loginResponse) {
        Toast.makeText(this, "" + loginResponse.getUserMobileno() + "\n   " + loginResponse.getUserName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String successMessage) {
        Toast.makeText(this, "" + successMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String failure) {
        Toast.makeText(this, "" + failure, Toast.LENGTH_SHORT).show();
    }
}