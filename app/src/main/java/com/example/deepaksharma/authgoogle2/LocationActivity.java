package com.example.deepaksharma.authgoogle2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.sociallogin.Location.Location;

public class LocationActivity extends Location {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
    }

    public void checkLocation(View v) {
        getLocation();
//        getLocation(3,5,10);
    }

    @Override

    public void getCurrentLocation(@NonNull double latitude, @NonNull double longitude, @NonNull String address) {
        super.getCurrentLocation(latitude, longitude, address);
    }

    //    @Override
//    public void getCurrentLocation(@NonNull String latitude, @NonNull String longitude,  @NonNull String currentAddress) {
//        super.getCurrentLocation(latitude,longitude, currentAddress);
//        Toast.makeText(getApplicationContext(), "Location changed ! Address -> " + currentAddress+
//                "\n alt->  " + location.getAltitude() + "\n getLatitude -> " + String.valueOf(location.getLatitude()) +
//                "\n  getLongitude -> " + String.valueOf(location.getLongitude()), Toast.LENGTH_SHORT).show();
//
//    }
}
