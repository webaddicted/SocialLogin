package com.example.sociallogin;

import android.content.Context;
import android.location.LocationManager;
import android.support.annotation.NonNull;

import com.example.sociallogin.Enum.LoginType;
import com.example.sociallogin.FormValidation.FormValidation;
import com.facebook.login.LoginManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SocialLogin {
    private static FirebaseAuth mAuth;
    private static LoginType mLoginType;
    private static Context mContext;
    private static String preferenceKey;

    /**
     * initialize Firebase
     */
    public static FirebaseAuth init(@NonNull Context context) {
        mContext = context;
        if (mAuth == null) {
            FirebaseApp.initializeApp(context);
            mAuth = FirebaseAuth.getInstance();
            FormValidation.init(context);
        }
        return mAuth;
    }

    public static FirebaseAuth getFirebaseAuth() {
        return mAuth;
    }

    /**
     * check user signIn status
     */
    public static boolean checkUserSignIn() {
        if (mAuth != null) {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser == null) return false;
            else return true;
        }
        return false;
    }

    /**
     * user signOut
     */
    public static boolean signOut() {
        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();
        return true;
    }

    public static void loginType(LoginType loginType) {
        mLoginType = loginType;
    }

    /**
     * Check LoginType
     */
    public static LoginType getloginType() {
        return mLoginType;
    }

    public static Context getContext() {
        return mContext;
    }

    public static void setPreferenceKey(String key) {
        preferenceKey = key;
    }

    public static String getPreferenceKey() {
        return preferenceKey;
    }

}
