package com.example.sociallogin.FormValidation;


import android.content.Context;
import android.util.Patterns;

import com.example.sociallogin.R;
import com.example.sociallogin.SocialLogin;

public class FormValidation {
    private static Context appContext = SocialLogin.getContext();
    private static FormValidation instance;
    public final static int PASSWORD_TYPE_ALPHA_NUMERIC_WITHOUT_SPACE = 1;
    public final static int PASSWORD_TYPE_ALPHA_NUMERIC_WITH_SPACE = 2;
    public final static int PASSWORD_TYPE_ALPHA_NUMERIC_SPECIAL_WITH_SPACE = 3;
    public final static int PASSWORD_TYPE_ALPHA_NUMERIC_SPECIAL_WITHOUT_SPACE = 4;

    public final static int PASSWORD_TYPE_STRICTLY_ALPHA_NUMERIC_WITHOUT_SPACE = 10;
    public final static int PASSWORD_TYPE_STRICTLY_ALPHA_NUMERIC_WITH_SPACE = 11;
    public final static int PASSWORD_TYPE_STRICTLY_ALPHA_NUMERIC_SPECIAL = 12;

    public final static int NAME_TYPE_FIRST_NAME = 20;
    public final static int NAME_TYPE_MIDDLE_NAME = 21;
    public final static int NAME_TYPE_LAST_NAME = 22;

    private final static String regex_singleSpacedWords = "^[a-zA-Z]+(\\s[a-zA-Z]+)*$";
    private final static String regex_alphaNumericWithSpace = "^[a-zA-z0-9 ]*";
    private final static String regex_alphaNumericWithoutSpace = "^[a-zA-Z0-9]*";
    private final static String regex_alphaNumericSpecialWithSpace = "^[ !\"#$%&'()*+,\\-./:;<=>?@\\[\\]^_`{|}~a-zA-Z0-9]*";

    private final static String regex_alphaNumericSpecialWithoutSpace = "^[!\"#$%&'()*+,\\-./:;<=>?@\\[\\]^_`{|}~a-zA-Z0-9]*";
    //Strictly alpha numeric.
    private final static String regex_atLeastAlphaInAlphaNumericWithoutSpace = "^([a-z|A-z|0-9])*[a-zA-Z]([a-z|A-z|0-9])*$";
    private final static String regex_atLeastNumericInAlphaNumericWithoutSpace = "^([a-z|A-z|0-9])*[0-9]([a-z|A-z|0-9])*$";
    private final static String regex_atLeastAlphaInAlphaNumericWithSpace = "^([a-z|A-z|0-9| ])*[a-zA-Z]([a-z|A-z|0-9| ])*$";
    private final static String regex_atLeastNumericInAlphaNumericWithSpace = "^([a-z|A-z|0-9| ])*[0-9]([a-z|A-z|0-9| ])*$";
    private final static String regex_atLeastAlpha = "^.*[a-zA-Z].*$";
    private final static String regex_atLeastNumeric = "^.*[0-9].*$";

    private final static String regex_atLeastSpecial = "^.*[!\"#$%&'()*+,\\-./:;<=>?@\\[\\]^_`{|}~].*$";

    /*
    Initialize validation library with Context(preferably application’s).

    * @param context object of class Context
    */
    public static void init(Context context) {
        appContext = context.getApplicationContext();
        instance = new FormValidation();
    }

    /*
    FormValidation is a singleton, this function provides an instance of FormValidation.
    */
    public static FormValidation getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new FormValidation();
            return instance;
        }
    }

    /*
    Validates name.
    * @param name name of a person
    * @param type type of the name. Types can be as follows
        NAME_TYPE_FIRST_NAME
            Return true if the name contains only alphabets and its length is between
            #first_name_min_length and #first_name_max_length.
        NAME_TYPE_MIDDLE_NAME
            Return true if name contains only alphabets and its length is between
            #middle_name_min_length and #middle_name_min_length.
        NAME_TYPE_LAST_NAME
            Return true if name contains only alphabets and its length is between
            #last_name_min_length and #last_name_max_length.
     * @return boolean  Returns true if firstName contains only alphabets and its length is between
                        #min_first_name_size and #max_first_name_size. Returns false otherwise.
     */
    public boolean isValidName(String name, int type) {
        int minSize = 0, maxSize = 0;
        switch (type) {
            case NAME_TYPE_FIRST_NAME:
                minSize = appContext.getResources().getInteger(R.integer.first_name_min_length);
                maxSize = appContext.getResources().getInteger(R.integer.first_name_max_length);
                break;
            case NAME_TYPE_MIDDLE_NAME:
                minSize = appContext.getResources().getInteger(R.integer.middle_name_min_length);
                maxSize = appContext.getResources().getInteger(R.integer.middle_name_max_length);
                break;
            case NAME_TYPE_LAST_NAME:
                minSize = appContext.getResources().getInteger(R.integer.last_name_min_length);
                maxSize = appContext.getResources().getInteger(R.integer.last_name_max_length);
                break;
            default:
                return false;
        }

        if (minSize == 0 && maxSize == 0)
            return false;
        else
            return isValidName(name, minSize, maxSize);
    }

    /*
     Validates full name.
    * @param fullName full name of a person
    * @return boolean Returns true if fullName contains only alphabets with non-consecutive spaces
                      and its length is between #min_full_name_size and #max_full_name_size. Returns
                      false otherwise.
    */
    public boolean isValidFullName(String fullName) {
        int minSize = appContext.getResources().getInteger(R.integer.full_name_min_length);
        int maxSize = appContext.getResources().getInteger(R.integer.full_name_max_length);
        if (isNotEmptyAfterTrimming(fullName)) {
            if (fullName.length() >= minSize && fullName.length() <= maxSize) {
                return fullName.matches(regex_singleSpacedWords);
            } else
                return false;
        } else
            return false;
    }

    /*
    Validates email format.
    * @param email
    * @return boolean Returns true if email is in proper format. Length of the string should be
                      between #email_min_length and #email_max_length. Returns false otherwise.
    */
    public boolean isValidEmail(String email) {
        int minSize = appContext.getResources().getInteger(R.integer.email_min_length);
        int maxSize = appContext.getResources().getInteger(R.integer.email_max_length);
        if (isNotEmptyAfterTrimming(email)) {
            if (email.length() >= minSize && email.length() <= maxSize) {
                return Patterns.EMAIL_ADDRESS.matcher(email).matches();

            } else
                return false;
        }
        return false;
    }

    /*
    Validates phone number format.
    * @param phoneNumber
    * @return boolean Return true if phoneNumber contains only numbers and length of phoneNumber is
                     between #phoneNumber_min_length and #phoneNumber_max_length.
    * */
    public boolean isValidPhoneNumber(String phoneNumber) {
        int minSize = appContext.getResources().getInteger(R.integer.phone_number_min_length);
        int maxSize = appContext.getResources().getInteger(R.integer.phone_number_max_length);
        if (isNotEmptyAfterTrimming(phoneNumber)) {
            if (phoneNumber.length() >= minSize && phoneNumber.length() <= maxSize) {
                char[] chars = phoneNumber.toCharArray();
                for (char c : chars) {

                    if (!Character.isDigit(c)) {
                        return false;
                    }
                }
                return true;
            } else
                return false;
        } else
            return false;
    }

    public boolean isValidPassword(String password, int type) {
        int minSize = appContext.getResources().getInteger(R.integer.password_min_length);
        int maxSize = appContext.getResources().getInteger(R.integer.password_max_length);
        if (password != null && !password.isEmpty()) {
            if (password.length() >= minSize && password.length() <= maxSize) {
                switch (type) {
                    case PASSWORD_TYPE_ALPHA_NUMERIC_WITHOUT_SPACE:
                        return password.matches(regex_alphaNumericWithoutSpace);
                    case PASSWORD_TYPE_ALPHA_NUMERIC_WITH_SPACE:
                        return password.matches(regex_alphaNumericWithSpace);
                    case PASSWORD_TYPE_ALPHA_NUMERIC_SPECIAL_WITH_SPACE:
                        return password.matches(regex_alphaNumericSpecialWithSpace);
                    case PASSWORD_TYPE_ALPHA_NUMERIC_SPECIAL_WITHOUT_SPACE:
                        return password.matches(regex_alphaNumericSpecialWithoutSpace);
                    case PASSWORD_TYPE_STRICTLY_ALPHA_NUMERIC_WITHOUT_SPACE:
                        return isStrictlyAlphaNumeric(password, false);
                    case PASSWORD_TYPE_STRICTLY_ALPHA_NUMERIC_WITH_SPACE:
                        return isStrictlyAlphaNumeric(password, true);
                    case PASSWORD_TYPE_STRICTLY_ALPHA_NUMERIC_SPECIAL:
                        return isStrictlyAlphaNumericSpecial(password);
                    default:
                        return false;
                }
            } else
                return false;
        } else
            return false;
    }

    /*
    Validates user name.
    * @param username
    * @return boolean Return true if username contains only numbers, letters or special characters(space not allowed)
                      and length of username is between #username_min_length and #username_max_length. Returns false otherwise.
     */
    public boolean isValidUsername(String username) {
        int minSize = appContext.getResources().getInteger(R.integer.username_min_length);
        int maxSize = appContext.getResources().getInteger(R.integer.username_max_length);
        if (isNotEmptyAfterTrimming(username)) {
            if (username.length() >= minSize && username.length() <= maxSize) {
                return username.matches(regex_alphaNumericSpecialWithoutSpace);
            } else return false;
        } else return false;

    }


    //Private functions
    private boolean isValidName(String name, int minSize, int maxSize) {
        if (isNotEmptyAfterTrimming(name)) {
            if (name.length() >= minSize && name.length() <= maxSize) {
                char[] chars = name.toCharArray();

                for (char c : chars) {
                    if (!Character.isLetter(c)) {
                        return false;
                    }
                }
                return true;
            } else
                return false;
        } else
            return false;
    }

    public boolean isNotEmptyAfterTrimming(String string) {
        if (string != null && !string.isEmpty()) {
            if (!string.trim().isEmpty())
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    private boolean passwordMatchRegex(String password, String regex) {
        return password.matches(regex);
    }

    private boolean isStrictlyAlphaNumeric(String password, boolean isSpaceAllowed) {
        if (isSpaceAllowed)
            return password.matches(regex_atLeastAlphaInAlphaNumericWithSpace)
                    && password.matches(regex_atLeastNumericInAlphaNumericWithSpace);
        else
            return password.matches(regex_atLeastAlphaInAlphaNumericWithoutSpace)
                    && password.matches(regex_atLeastNumericInAlphaNumericWithoutSpace);
    }

    private boolean isStrictlyAlphaNumericSpecial(String password) {
        return password.matches(regex_atLeastAlpha)
                && password.matches(regex_atLeastNumeric)
                && password.matches(regex_atLeastSpecial);
    }

}