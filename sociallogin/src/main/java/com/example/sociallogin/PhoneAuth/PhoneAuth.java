package com.example.sociallogin.PhoneAuth;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.sociallogin.FormValidation.FormValidation;
import com.example.sociallogin.Interface.OnLoginListener;
import com.example.sociallogin.Model.LoginResponse;
import com.example.sociallogin.SocialLogin;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class PhoneAuth {
    private static String TAG = PhoneAuth.class.getSimpleName();
    private static Activity mActivity;
    private static String mVerificationId;
    private static OnLoginListener mLoginResponse;
    private static PhoneAuthProvider.ForceResendingToken mResendToken;
    private static FirebaseAuth mAuth = SocialLogin.getFirebaseAuth();
    private static PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    /**
     * @param activity referance of activity
     * @param onLoginListener is check phone auth status
     */
    public static void phoneAuthCallBack(@NonNull Activity activity, @NonNull OnLoginListener onLoginListener) {
        mActivity = activity;
        mLoginResponse = onLoginListener;
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
//            init phone no of fcm
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    mLoginResponse.onFailure(e.toString());
                    Log.w(TAG, "onVerificationFailed -> Invalid phone number ", e);
                } else {
                    mLoginResponse.onFailure(e.toString());
                    Log.d(TAG, "onVerificationFailed: Auth " + e);
                }
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
                mLoginResponse.onSuccess("Success");
            }
        };
    }
    /**
     * send OTP to mobile no
     * @param phoneNumber is entered mobile no
     */
    public static void sendOTP(@NonNull String phoneNumber) {
        StringBuilder sb = new StringBuilder();
        if(!phoneNumber.contains("+91")) {
            sb.append("+91");
            sb.append(phoneNumber);
        }
        if (FormValidation.getInstance().isValidPhoneNumber(phoneNumber)) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(sb+"",        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    mActivity,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
            mLoginResponse.onFailure("Success");
        } else {
            mLoginResponse.onFailure("Enter valid mobile number.");
        }
    }
    /**
     * verify sent OTP by checking same mVerfication ID
     * @param code is entered mobile no
     */
    public static void verifyOTP(@NonNull String code) {
        if (code != null && mVerificationId != null) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        } else {
            if (mVerificationId == null) mLoginResponse.onFailure("Verification id not found");
            else mLoginResponse.onFailure("Please enter valid code");
        }
    }
    /**
     * resend OTP on mobile No
     * @param phoneNumber is entered mobile no
     */
    public static void resendOTP(@NonNull String phoneNumber) {
        StringBuilder sb = new StringBuilder();
        if(!phoneNumber.contains("+91")) {
            sb.append("+91");
            sb.append(phoneNumber);
        }
        if (FormValidation.getInstance().isValidPhoneNumber(phoneNumber)) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(sb+"",        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    mActivity,               // Activity (for callback binding)
                    mCallbacks,         // OnVerificationStateChangedCallbacks
                    mResendToken);             // ForceResendingToken from callbacks
            mLoginResponse.onFailure("Success");
        } else {
            mLoginResponse.onFailure("Enter valid mobile number.");
        }
    }
    /**
     * verified user no and registered on fcm.
     * @param credential is Authorized Credential of mobile no.
     */
    private static void signInWithPhoneAuthCredential(@NonNull PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "signInWithCredential:success");
                    FirebaseUser user = task.getResult().getUser();
                    getUserInfo(user);
                } else {
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                    mLoginResponse.onFailure(task.getException().toString());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        Log.d(TAG, "Invalid code.");
                    }
                }
            }
        });
    }
    /**
     * @param  firebaseUser is current login user
     */
    private static void getUserInfo(@NonNull FirebaseUser firebaseUser) {
        LoginResponse loginResponse = new LoginResponse(firebaseUser.getUid(), firebaseUser.getDisplayName(), firebaseUser.getEmail(), firebaseUser.getPhoneNumber(), firebaseUser.getPhotoUrl() + "", firebaseUser.getProviderId());
        mLoginResponse.onSuccess(loginResponse);
    }
}
