package com.example.sociallogin.Permission;

public interface PermissionListener {
    void onPermissionGranted();

    void onPermissionDenied();
}
